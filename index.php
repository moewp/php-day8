<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>OOP PHP</title>
</head>

<body>
      <h1>Object Oriented Programming</h1>
</body>

</html>

<?php

require_once('animals.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animals("Shaunn");
echo "Nama Hewan: " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki: " . $sheep->legs . "<br>"; // 2
echo "Apakah berdarah dingin? " . $sheep->cold_blooded . "<br>"; // false

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Suara Ape: " . $sungokong->yell() . "<br>"; // "Auooo"

$kodok = new Frog("buduk");
echo "Suara Kodok Lompat: " . $kodok->jump() . "<br>"; // "hop hop"
?>